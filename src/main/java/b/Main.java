package b;

import a.MyThread;

import javax.swing.*;


public class Main {
  static Thread th;
  static Thread th2;

  public static void main(String[] args) {
    JFrame win = new JFrame();
    win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    win.setSize(500, 400);

    JPanel panel = getJPanel();

    win.setContentPane(panel);
    win.setVisible(true);
  }

  private static JPanel getJPanel() {
    JPanel panel = new JPanel();
    JButton btnFirst = new JButton("Start/stop first thread");
    JButton btnSecond = new JButton("Start/stop second thread");
    JSlider slider = new JSlider();
    MyThread.slider = slider;
    slider.setValue(50);


    btnFirst.addActionListener(e -> {
      if (th != null && th.isAlive()) {
        MyThread.synchronize = true;
        th.stop();
        return;
      }
      th = new Thread(new MyThread(10));
      th.setPriority(1);
      th.start();
    });

    btnSecond.addActionListener(e -> {
      if (th2 != null && th2.isAlive()) {
        MyThread.synchronize = true;
        th2.stop();
        return;
      }
      th2 = new Thread(new MyThread(90));
      th2.setPriority(10);
      th2.start();
    });


    panel.add(btnFirst);
    panel.add(btnSecond);
    panel.add(slider);
    return panel;
  }


}
