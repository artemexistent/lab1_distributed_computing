package a;

import javax.swing.*;

public class MyThread extends Thread {

  public static JSlider slider;
  public static boolean synchronize = true;
  private final int size;

  public MyThread(int size) {
    this.size = size;
  }

  @Override
  public void run() {
    if (!synchronize) {
      return;
    }
    synchronize = false;
    int nowPosition;
    while ((nowPosition = slider.getValue()) != size) {
      slider.setValue(nowPosition > size ? nowPosition - 1 : nowPosition + 1);
      try {
        Thread.sleep(100);
      } catch (InterruptedException interruptedException) {
        interruptedException.printStackTrace();
      }
    }
    synchronize = true;
  }

}
