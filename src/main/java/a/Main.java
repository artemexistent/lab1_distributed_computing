package a;

import javax.swing.*;


public class Main {
  static Thread th;
  static Thread th2;

  public static void main(String[] args) {
    JFrame win = new JFrame();
    win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    win.setSize(500, 400);

    JPanel panel = getJPanel();

    win.setContentPane(panel);
    win.setVisible(true);
  }

  private static JPanel getJPanel() {
    JPanel panel = new JPanel();
    JButton btn = new JButton("Start thread");
    JSlider slider = new JSlider();
    MyThread.slider = slider;
    slider.setValue(50);
    String[] priorities = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

    JComboBox<String> comboBoxForFirstThread = new JComboBox<>(priorities);
    JComboBox<String> comboBoxForSecondThread = new JComboBox<>(priorities);

    comboBoxForFirstThread.addActionListener(e -> th.setPriority(comboBoxForFirstThread.getSelectedIndex() + 1));
    comboBoxForSecondThread.addActionListener(e -> th2.setPriority(comboBoxForSecondThread.getSelectedIndex() + 1));

    th = new Thread(new MyThread(10));
    th2 = new Thread(new MyThread(90));

    btn.addActionListener(e -> {
      if (th.isAlive() || th2.isAlive()) {
        System.out.println(th.getPriority());
        System.out.println(th2.getPriority());
        System.out.println();
        return;
      }
      th2.start();
      th.start();
    });


    panel.add(comboBoxForFirstThread);
    panel.add(comboBoxForSecondThread);

    panel.add(btn);
    panel.add(slider);
    return panel;
  }


}
